import {Msg, MsgAuth, MsgDownload, MsgError, MsgListDirectory, MsgShareKey, MsgStartUpload, MsgUploadComplete} from './app.model';
import {Subscriber} from 'rxjs/Subscriber';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import {BSON} from 'bson';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import {Subject} from 'rxjs/Subject';

export abstract class CoffreWebsocket {
  protected bson = new BSON();
  private readonly url: string;
  private socket$: WebSocketSubject<any>;
  private pendingPackets: {
    observer: Subscriber<Msg>,
    packet: { action: string, operationId: number, data: any },
    operationId: number
  }[] = [];
  private sentPackets: { observer: Subscriber<Msg>, operationId: number }[] = [];
  private maxSentPacket = 1;
  private nextOperationId = 1;

  private onConnectionEstablished: Subject<Event> = new Subject();
  private onConnectionClosed: Subject<CloseEvent> = new Subject();

  protected constructor(url: string) {
    this.url = url;
    this.socket$ = webSocket({
      url: this.url,
      binaryType: 'arraybuffer',
      openObserver: this.onConnectionEstablished,
      closeObserver: this.onConnectionClosed,
      deserializer: data => {
        return this.bson.deserialize(Buffer.from(data.data));
      },
      serializer: value => {
        return this.bson.serialize(value);
      }
    });
  }

  public connect() {
    this.socket$.subscribe(
      (data) => {
        this.onPacket(data);
      },
      error => {
        this.onError(error);
      }
    );
  }

  public sendPacket<T extends Msg>(action: string, data: any): Observable<T> {
    return Observable.create((observer: Subscriber<T>) => {
      let operationId = this.nextOperationId++;
      this.pendingPackets.push({
        packet: {
          action: action,
          operationId: operationId,
          data: data
        },
        operationId: operationId,
        observer: observer
      });
      this.sendPendingPackets();
    });
  }

  private sendPendingPackets(): void {
    if (this.sentPackets.length < this.maxSentPacket) {
      let nextPacket = this.pendingPackets.shift();
      if (!nextPacket) {
        return;
      }
      this.socket$.next(nextPacket.packet);
      delete nextPacket.packet;
      this.sentPackets.push(nextPacket);
    }
  }

  protected onMessage(msg: Msg): void {
    let sentPacket = this.sentPackets.find(p => p.operationId === msg.operationId);
    if (!sentPacket) {
      console.error('Received a message with an unknown operationId', msg);
      return;
    }

    if (msg instanceof MsgError) {
      sentPacket.observer.error(new Error(msg.errorMessage));
    } else {
      sentPacket.observer.next(msg);
      if (msg.completed) {
        sentPacket.observer.complete();
        let i = this.sentPackets.findIndex((p) => p.operationId === msg.operationId);
        this.sentPackets.splice(i, 1);
      }
    }

    this.sendPendingPackets();
  }

  protected onError(error: Error): void {
    for (let sentPacket of this.sentPackets) {
      sentPacket.observer.error(error);
    }
    this.close();
  }

  protected abstract onPacket(data: any): void;

  public registerClose(): Observable<CloseEvent> {
    return this.onConnectionClosed;
  }

  public registerConnect(): Observable<Event> {
    return this.onConnectionEstablished;
  }

  public close() {
    this.socket$.complete();
  }
}

export class ControlCoffreWebsocket extends CoffreWebsocket {
  public constructor(url: string) {
    super(url);
  }

  protected onPacket(response: any): void {
    let msg: Msg;
    switch (response.action) {
      case 'error':
        msg = MsgError.fromJson(response);
        break;
      case 'create-safe':
      case 'auth':
        msg = MsgAuth.fromJson(response);
        break;
      case 'share-key':
        msg = MsgShareKey.fromJson(response);
        break;
      case 'list-directory':
        msg = MsgListDirectory.fromJson(response);
        break;
      case 'delete-file':
        msg = Msg.fromJson(response);
        break;
      default:
        console.error('received unsupported action: `' + response.action + '\'');
        this.close();
        return;
    }
    super.onMessage(msg);
  }
}

export class UploadCoffreWebsocket extends CoffreWebsocket {
  public constructor(url: string) {
    super(url);
  }

  protected onPacket(response: any): void {
    let msg: Msg;
    switch (response.action) {
      case 'error':
        msg = MsgError.fromJson(response);
        break;
      case 'auth':
        msg = MsgAuth.fromJson(response);
        break;
      case 'start-upload':
        msg = MsgStartUpload.fromJson(response);
        break;
      case 'upload-complete':
        msg = MsgUploadComplete.fromJson(response);
        break;
      case 'upload':
      case 'cancel-upload':
        msg = Msg.fromJson(response);
        break;
      default:
        console.error('received unsupported action: `' + response.action + '\'');
        this.close();
        return;
    }
    super.onMessage(msg);
  }

  public sendStartUploadFile(filename: string, directoryUid: string) {
    return this.sendPacket('start-upload', {
      filename: filename,
      directory: directoryUid
    });
  }

  public sendUploadFile(data: any): Observable<Msg> {
    return this.sendPacket('upload', {
      data: data
    });
  }

  public sendUploadComplete() {
    return this.sendPacket('upload-complete', {});
  }

  public sendCancelUploadFile() {
    return this.sendPacket('cancel-upload', {});
  }
}


export class DownloadCoffreWebsocket extends CoffreWebsocket {
  public constructor(url: string) {
    super(url);
  }

  protected onPacket(response: any): void {
    let msg: Msg;
    switch (response.action) {
      case 'error':
        msg = MsgError.fromJson(response);
        break;
      case 'auth':
        msg = MsgAuth.fromJson(response);
        break;
      case 'download':
        msg = MsgDownload.fromJson(response);
        break;
      case 'start-download':
      case 'cancel-download':
        msg = Msg.fromJson(response);
        break;
      default:
        console.error('received unsupported action: `' + response.action + '\'');
        this.close();
        return;
    }
    super.onMessage(msg);
  }

  public sendStartDownload(fileuid: string) {
    return this.sendPacket('download', {
      uid: fileuid
    });
  }
}
