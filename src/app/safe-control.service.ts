import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {ControlCoffreWebsocket} from './coffre-websocket';
import {environment} from '../environments/environment';
import {Msg, MsgAuth, MsgListDirectory, MsgShareKey} from './app.model';
import {CoffreDirectory} from './files.model';

export enum SafeState {
  Disconnected = 'Disconnected',
  Disconnecting = 'Disconnecting',
  Connecting = 'Connecting',
  WaitingAuth = 'WaitingAuth',
  Authenticating = 'Authenticating',
  Creating = 'Creating',
  Connected = 'Connected',
}

@Injectable()
export class SafeControlService {
  get state(): SafeState {
    return this._state;
  }

  private _state: SafeState = SafeState.Disconnected;
  private _controlWebsocket?: ControlCoffreWebsocket;

  constructor() {
  }

  connect(): Observable<Event> {
    this.ensureState(SafeState.Disconnected);
    this._controlWebsocket = new ControlCoffreWebsocket(environment.wsUrl);

    this._controlWebsocket.registerClose().subscribe((_: CloseEvent) => {
      this._state = SafeState.Disconnected;
    });

    this._controlWebsocket.registerConnect().subscribe((_: Event) => {
      this._state = SafeState.WaitingAuth;
    });

    this._controlWebsocket.connect();
    this._state = SafeState.Connecting;

    return this._controlWebsocket.registerConnect();
  }

  private ensureState(state: SafeState): void {
    if (this._state !== state) {
      throw new Error(`Invalid state: Expected ${state} but was ${this._state}`);
    }
  }

  createSafe(name: string, passphrase: string): Observable<MsgAuth> {
    this.ensureState(SafeState.WaitingAuth);
    this._state = SafeState.Creating;
    return Observable.create((observer) => {
      this.sendPacket<MsgAuth>('create', {
          name: name,
          passphrase: passphrase
        }
      ).subscribe((msg: MsgAuth) => {
        if (msg.success) {
          CoffreDirectory.maxFilePerDirectory = msg.max_file_per_directory;
          this._state = SafeState.Connected;
          observer.next(msg);
        } else {
          this.close();
          observer.error(msg);
        }
        observer.complete();
      });
    });
  }

  authenticate(name: string, passphrase: string): Observable<MsgAuth> {
    this.ensureState(SafeState.WaitingAuth);
    this._state = SafeState.Authenticating;
    return Observable.create((observer) => {
      this.sendPacket<MsgAuth>('auth', {
          name: name,
          passphrase: passphrase
        }
      ).subscribe((msg: MsgAuth) => {
        if (msg.success) {
          CoffreDirectory.maxFilePerDirectory = msg.max_file_per_directory;
          this._state = SafeState.Connected;
          observer.next(msg);
        } else {
          this.close();
          observer.error(msg);
        }
        observer.complete();
      });
    });
  }

  listDirectory(directoryUid: string): Observable<MsgListDirectory> {
    this.ensureState(SafeState.Connected);

    return Observable.create((observer) => {
      this.sendPacket('list-directory', {
          uid: directoryUid
        }
      ).subscribe((msg: MsgAuth) => {
        if (msg.success) {
          observer.next(msg);
        } else {
          this.close();
          observer.error(msg);
        }
        observer.complete();
      });
    });
  }

  shareKey(mode: number): Observable<MsgShareKey> {
    this.ensureState(SafeState.Connected);
    return Observable.create((observer) => {
      this.sendPacket('share-key', {
          mode: mode
        }
      ).subscribe((msg: MsgAuth) => {
        if (msg.success) {
          observer.next(msg);
        } else {
          this.close();
          observer.error(msg);
        }
        observer.complete();
      });
    });
  }

  deleteFile(directoryUid: string, fileUid: string): Observable<Msg> {
    this.ensureState(SafeState.Connected);

    return Observable.create((observer) => {
      this.sendPacket('delete-file', {
          directoryUid: directoryUid,
          fileUid: fileUid
        }
      ).subscribe((msg: Msg) => {
        if (msg.success) {
          observer.next(msg);
        } else {
          this.close();
          observer.error(msg);
        }
        observer.complete();
      });
    });
  }

  sendPacket<T extends Msg>(action: string, data: any): Observable<T> {
    if (this._controlWebsocket === undefined) {
      throw new Error('Control socket should be open before sending packet');
    }
    return this._controlWebsocket.sendPacket<T>(action, data);
  }

  close(): void {
    if (!this._controlWebsocket) {
      return;
    }
    this._state = SafeState.Disconnecting;
    this._controlWebsocket.close();
  }
}
