import {Injectable} from '@angular/core';
import {CoffrePlainFile, FileState} from './files.model';
import {Msg, MsgAuth, MsgDownload} from './app.model';
import {DownloadCoffreWebsocket} from './coffre-websocket';
import {SafeControlService} from './safe-control.service';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class DownloadFilesService {
  private downloadWebsocket?: DownloadCoffreWebsocket;
  private pendingFiles: CoffrePlainFile[] = [];
  private currentFile?: CoffrePlainFile;
  private nextId = 1;
  public currentFileState?: FileState = undefined;
  private currentFileData: Buffer[] = [];

  constructor(private _safeControlService: SafeControlService) {
  }

  private connect(): Observable<Msg> {
    if (this.downloadWebsocket) {
      throw new Error('A connection is already established');
    }
    return Observable.create((observer => {
      this._safeControlService.shareKey(3).subscribe((msg) => {
        this.downloadWebsocket = new DownloadCoffreWebsocket(environment.wsUrl);

        this.downloadWebsocket.registerClose().subscribe((ev: CloseEvent) => {
          this.clear();
        });

        this.downloadWebsocket.registerConnect().subscribe((ev: Event) => {
          if (!this.downloadWebsocket) {
            observer.error(new Error('undefined upload websocket'));
            return;
          }
          this.downloadWebsocket.sendPacket('auth-token', {token: msg.token}).subscribe((msgAuth: MsgAuth) => {
            if (msgAuth.success) {
              observer.next(msgAuth);
              observer.complete();
            } else {
              observer.error(new Error('Failed to auth'));
            }
          }, err => {
            observer.error(err);
          });
        }, err => {
          observer.error(err);
        });
      });
    }));
  }

  public clear() {
    this.downloadWebsocket = undefined;
    this.currentFile = undefined;
    this.pendingFiles = [];
  }

  public downloadFile(file: CoffrePlainFile): void {
    this.pendingFiles.push(file);

    if (!this.currentFile) {
      this.startDownload();
    }
  }

  private startDownload() {
    if (!this.downloadWebsocket) {
      this.connect().subscribe((msg: Msg) => {
        this.startDownload();
      });
      return;
    }

    this.currentFile = this.pendingFiles.shift();
    if (!this.currentFile) {
      return;
    }

    if (!this.currentFile.parent) {
      throw new Error('Missing parent');
    }

    this.currentFileState = new FileState(this.nextId++, this.currentFile.name, this.currentFile.parent, this.currentFile.length);
    this.downloadWebsocket.sendStartDownload(this.currentFile.uid).subscribe((msg: Msg) => {
      this.onStartDownload(msg);
    });
  }

  private browserDownload() {
    if (!this.currentFileState) {
      throw new Error('No current file state');
    }

    let a = window.document.createElement('a');
    a.href = window.URL.createObjectURL(new Blob(this.currentFileData, {type: 'application/octet-stream'}));
    a.download = this.currentFileState.name;

    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  private onStartDownload(msg: Msg | MsgDownload) {
    if (!this.currentFile) {
      return;
    }

    if (!msg.success) {
      // FIXME: notify error
      return;
    }

    if (msg instanceof MsgDownload) {
      if (msg.data.buffer.length === 0) {
        this.browserDownload();
        this.currentFileData = [];
      } else {
        this.currentFileData.push(msg.data.buffer);
      }
    }
  }
}
