import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {CoffreMaterialModule} from './coffre-material.module';

import {AppComponent} from './app.component';
import {FileListComponent} from './file-list.component';
import {DropFileDirective} from './drop-file.directive';
import {UploadFilesService} from './upload-files.service';
import {UploadStatesComponent} from './upload-states.component';
import {FilesService} from './files-service';
import {FileSizePipe} from './file-size.pipe';
import {SafeControlService} from './safe-control.service';
import {DownloadFilesService} from './download-file.service';

@NgModule({
  declarations: [
    AppComponent,
    FileListComponent,
    DropFileDirective,
    UploadStatesComponent,
    FileSizePipe
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoffreMaterialModule
  ],
  providers: [UploadFilesService, DownloadFilesService, FilesService, SafeControlService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
