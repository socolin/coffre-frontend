import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'coffreFileSize'})
export class FileSizePipe implements PipeTransform {
  private units = ['B', 'KB', 'MB', 'GB', 'TB'];

  transform(value: any, ...args: any[]): any {
    let unit = 0;
    if (typeof(value) === 'string') {
      value = parseFloat(value);
    }
    while (value > 1024 && unit < this.units.length) {
      value /= 1024;
      unit++;
    }

    return value.toFixed(1) + ' ' + this.units[unit];
  }
}
