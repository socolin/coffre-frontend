import {Directive, ElementRef, EventEmitter, HostListener, Input, Output, Renderer2} from '@angular/core';

@Directive({
  selector: '[coffreDropFile]'
})
export class DropFileDirective {
  @Output() public filesDropped: EventEmitter<{files: FileList, directoryUid: string}> = new EventEmitter();
  @Input() public directoryUid: string;

  constructor(private renderer: Renderer2,
              private elementRef: ElementRef) {
  }

  @HostListener('drop', ['$event'])
  public onDrop(event) {
    event.preventDefault();
    event.stopPropagation();

    this.renderer.removeClass(this.elementRef.nativeElement, 'dragover');
    let files = event.dataTransfer.files;
    if (files.length > 0) {
      this.filesDropped.emit({directoryUid: this.directoryUid, files: files});
    }
  }

  @HostListener('dragenter', ['$event'])
  public onDragEnter(event: DragEvent) {
    this.renderer.addClass(this.elementRef.nativeElement, 'dragover');
  }

  @HostListener('dragleave', ['$event'])
  public onDragLeave(event: DragEvent) {
    this.renderer.removeClass(this.elementRef.nativeElement, 'dragover');
  }

  @HostListener('dragover', ['$event'])
  public onDragOver(event: DragEvent) {
    event.dataTransfer.dropEffect = 'move';
    event.preventDefault();
    event.stopPropagation();
  }
}
