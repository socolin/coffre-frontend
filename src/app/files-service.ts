import {Injectable} from '@angular/core';
import {CoffreDirectory, CoffreFile, CoffrePlainFile} from './files.model';
import {Msg, MsgListDirectory} from './app.model';
import {UploadFilesService} from './upload-files.service';
import {SafeControlService} from './safe-control.service';

@Injectable()
export class FilesService {
  private directoriesByUid: { [uid: string]: CoffreDirectory } = {};
  rootDirectory?: CoffreDirectory = undefined;
  loadingRoot = false;
  loadingDirectory = false;

  constructor(private _safeControlService: SafeControlService,
              private _uploadFilesService: UploadFilesService) {
    this._uploadFilesService.onFileUploaded.subscribe((newFile: { file: CoffreFile, directoryUid: string, askedDirectoryUid: string }) => {
      let directory = this.directoriesByUid[newFile.directoryUid];
      if (!directory) {
        if (newFile.askedDirectoryUid === newFile.directoryUid) {
          console.log('Error: Invalid directoryUid: ' + newFile.directoryUid);
          return;
        }
        let previous = this.directoriesByUid[newFile.askedDirectoryUid];
        if (!previous) {
          console.log('Error: Invalid askedDirectoryUid: ' + newFile.askedDirectoryUid);
          return;
        }
        while (previous.nextDirectory) {
          previous = previous.nextDirectory;
        }
        directory = previous.setNextUid(newFile.directoryUid);
        console.log('new directory');
        this.directoriesByUid[directory.uid] = directory;
      }
      directory.addFile(newFile.file);
    });
  }

  clear() {
    this._uploadFilesService.clear();
    this.loadingRoot = false;
    this.rootDirectory = undefined;
    this.directoriesByUid = {};
  }

  upload(directoryUid: string, file: File) {
    let directory = this.directoriesByUid[directoryUid];
    this._uploadFilesService.addFile(directory, file);
  }

  loadRootDirectory() {
    this.loadingRoot = true;
    this.listDirectory('root');
  }

  listDirectory(uid: string) {
    this.loadingDirectory = true;
    this._safeControlService.listDirectory(uid).subscribe((msg: MsgListDirectory) => {
      this.handleListDirectoryMsg(msg);
    });
  }

  private handleListDirectoryMsg(msg: MsgListDirectory) {
    let directory: CoffreDirectory;

    if (this.loadingRoot) {
      this.loadingRoot = false;
      this.directoriesByUid = {};
      directory = new CoffreDirectory(msg.uid, msg.name, 0, undefined, undefined);
      this.directoriesByUid[msg.uid] = directory;
      this.rootDirectory = directory;
    } else {
      directory = this.directoriesByUid[msg.uid];
    }

    let files = CoffreFile.filesFromJson(msg.files, directory);
    for (let file of files) {
      if (file instanceof CoffreDirectory) {
        this.directoriesByUid[file.uid] = file;
      }
    }
    directory.addFiles(files);

    if (msg.next_uid) {
      let nextDirectory = directory.setNextUid(msg.next_uid);
      this.directoriesByUid[nextDirectory.uid] = nextDirectory;
      this._safeControlService.listDirectory(nextDirectory.uid).subscribe((m: MsgListDirectory) => {
        this.handleListDirectoryMsg(m);
      });
    } else {
      this.loadingDirectory = false;
    }
  }

  getDirectory(uid: string): CoffreDirectory | undefined {
    if (!(uid in this.directoriesByUid)) {
      return undefined;
    }
    return this.directoriesByUid[uid];
  }

  deleteFile(file: CoffrePlainFile) {
    file.markAsDeleted();
    this._safeControlService.deleteFile(file.parent!.uid, file.uid).subscribe((_: Msg) => {
      file.completeDelete();
    },
    (_: Msg) => {
      file.cancelDelete();
    });
  }
}
