import {Component, ViewChild} from '@angular/core';
import {UploadFilesService} from './upload-files.service';
import {Portal} from '@angular/cdk/portal';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {FilesService} from './files-service';

@Component({
  selector: 'coffre-upload-states',
  templateUrl: './upload-states.component.html',
  styleUrls: ['./upload-states.component.scss']
})
export class UploadStatesComponent {
  @ViewChild('uploadStatesDialog')
  public uploadStatesDialog: Portal<any>;
  public uploadStatesOverlayRef: OverlayRef;

  constructor(public uploadFileService: UploadFilesService,
              public filesService: FilesService,
              private _overlay: Overlay) {
  }

  onDrop(dropData: { files: FileList, directoryUid: string }) {
    for (let i = 0; i < dropData.files.length; i++) {
      let file = dropData.files[i];
      this.filesService.upload(dropData.directoryUid, file);
    }
  }

  openUploadStateDialog() {
    let config = new OverlayConfig();

    config.positionStrategy = this._overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();
    config.hasBackdrop = true;

    let overlayRef = this._overlay.create(config);
    overlayRef.attach(this.uploadStatesDialog);
    overlayRef.backdropClick().subscribe(() => overlayRef.detach());
    this.uploadStatesOverlayRef = overlayRef;
  }

  closeUploadStateDialog() {
    if (this.uploadStatesOverlayRef) {
      this.uploadStatesOverlayRef.detach();
    }
  }
}
