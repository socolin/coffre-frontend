import {Component, Input} from '@angular/core';

import {CoffreDirectory, CoffrePlainFile} from './files.model';
import {DownloadFilesService} from './download-file.service';
import {FilesService} from './files-service';

@Component({
  selector: 'coffre-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss']
})
export class FileListComponent {
  @Input() directoryData: CoffreDirectory;
  columnsToDisplay = ['type', 'name', 'date', 'length', 'action'];

  constructor(private _downloadFileService: DownloadFilesService,
              private _fileService: FilesService) {
  }

  public downloadFile(file: CoffrePlainFile) {
    this._downloadFileService.downloadFile(file);
  }

  public deleteFile(file: CoffrePlainFile) {
    this._fileService.deleteFile(file);
  }
}
