import {Binary} from 'bson';

export class Msg {
  action: string;
  operationId: number;
  success: boolean;
  completed: boolean;

  static fromJson(data: any): Msg {
    let msg = new Msg();
    Object.assign(msg, data);
    return msg;
  }
}

export class MsgError extends Msg {
  public errorMessage: string;

  static fromJson(data: any): MsgError {
    let msg = new MsgError();
    Object.assign(msg, data);
    return msg;
  }
}

export class MsgAuth extends Msg {
  public max_file_per_directory: number;

  static fromJson(data: any): MsgAuth {
    let msg = new MsgAuth();
    Object.assign(msg, data);
    return msg;
  }
}

export class MsgDownload extends Msg {
  public data: Binary;

  static fromJson(data: any): MsgDownload {
    let msg = new MsgDownload();
    Object.assign(msg, data);
    return msg;
  }
}

export class MsgFileData {
  uid: string;
  name: string;
  type: number;
  date: number;
  length: number;
}

export class MsgListDirectory extends Msg {
  uid: string;
  name: string;
  next_uid?: string;
  files: MsgFileData[];

  static fromJson(data: any): MsgListDirectory {
    let msg = new MsgListDirectory();
    Object.assign(msg, data);
    return msg;
  }
}

export class MsgStartUpload extends Msg {
  file_uid: string;

  static fromJson(data: any): MsgStartUpload {
    let msg = new MsgStartUpload();
    Object.assign(msg, data);
    return msg;
  }
}

export class MsgUploadComplete extends Msg {
  directory_uid: string;

  static fromJson(data: any): MsgUploadComplete {
    let msg = new MsgUploadComplete();
    Object.assign(msg, data);
    return msg;
  }
}

export class MsgShareKey extends Msg {
  token: string;

  static fromJson(data: any): MsgShareKey {
    let msg = new MsgShareKey();
    Object.assign(msg, data);
    return msg;
  }
}
