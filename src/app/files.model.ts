export class FileState {
  constructor(id: number, name: string, directory: CoffreDirectory, fileLength: number) {
    this.id = id;
    this.name = name;
    this.directory = directory;
    this.sizeBytes = fileLength;
  }

  readonly id: number;
  readonly name: string;
  readonly directory: CoffreDirectory;
  state = 'PENDING';
  uid: string;
  sizeBytes = 0;
  sent = 0;
  transferredBytes = 0;
  canceled = false;

  get percentage(): number {
    return Math.round(this.transferredBytes / this.sizeBytes * 100);
  }
}


export abstract class CoffreFile {
  public uid: string;
  public name: string;
  public date: number;
  public parent?: CoffreDirectory;
  public deleted = false;

  protected constructor(uid: string, name: string, date: number, parent?: CoffreDirectory) {
    this.uid = uid;
    this.name = name;
    this.date = date;
    this.parent = parent;
  }

  markAsDeleted() {
    this.deleted = true;
  }

  cancelDelete() {
    this.deleted = false;
  }

  completeDelete() {
    if (!this.parent) {
      return;
    }

    this.parent.removeFile(this);
  }

  static filesFromJson(filesData: any[], parent: CoffreDirectory): CoffreFile[] {
    let files: CoffreFile[] = [];

    for (let fileData of filesData) {
      switch (fileData.type) {
        case 1:
          files.push(CoffrePlainFile.fromJson(fileData, parent));
          break;
        case 2:
          files.push(CoffreDirectory.fromJson(fileData, parent));
          break;
      }
    }

    return files;
  }

  abstract getType(): string;
}

export class CoffrePlainFile extends CoffreFile {
  public length: number;

  constructor(uid: string, name: string, date: number, length: number, parent: CoffreDirectory) {
    super(uid, name, date, parent);
    this.length = length;
  }

  static fromJson(fileData: any, parent: CoffreDirectory): CoffrePlainFile {
    return new CoffrePlainFile(fileData.uid, fileData.name, fileData.date, fileData.length, parent);
  }

  getType(): string {
    return 'file';
  }
}

export class CoffreDirectory extends CoffreFile {
  public static maxFilePerDirectory: number;
  public nextUid?: string;
  public files: CoffreFile[] = [];
  public phyFiles: CoffreFile[] = [];
  public mainDirectory: CoffreDirectory;
  public nextDirectory?: CoffreDirectory;

  constructor(uid: string, name: string, date: number, parent?: CoffreDirectory, mainDirectory?: CoffreDirectory) {
    super(uid, name, date, parent);
    this.uid = uid;
    this.name = name;

    if (mainDirectory) {
      this.mainDirectory = mainDirectory;
    } else {
      this.mainDirectory = this;
    }
  }

  static fromJson(fileData: any, parent?: CoffreDirectory, main?: CoffreDirectory): CoffreDirectory {
    return new CoffreDirectory(fileData.uid, fileData.name, fileData.date, parent, main);
  }

  getType(): string {
    return 'directory';
  }

  public addFile(file: CoffreFile) {
    file.parent = this;
    this.addFiles([file]);
  }

  public addFiles(files: CoffreFile[]) {
    this.mainDirectory.files = this.mainDirectory.files.concat(files);
    this.phyFiles = this.phyFiles.concat(files);
  }

  public removeFile(file: CoffreFile) {
    let i = this.mainDirectory.files.findIndex(f => f.uid === file.uid);
    if (i !== -1) {
      this.mainDirectory.files.splice(i, 1);
    }
    i = this.phyFiles.findIndex(f => f.uid === file.uid);
    if (i !== -1) {
      this.phyFiles.splice(i, 1);
    }
  }

  setNextUid(nextUid: string): CoffreDirectory {
    this.nextUid = nextUid;
    this.nextDirectory = new CoffreDirectory(nextUid, this.name, this.date, this.parent, this.mainDirectory);
    return this.nextDirectory;
  }

  getChild(uid: string): CoffreFile | undefined {
    return this.files.find(f => f.uid === uid);
  }

  getChildDirectory(uid: string): CoffreDirectory | undefined {
    let file = this.files.find(f => f.uid === uid);
    if (file instanceof CoffreDirectory) {
      return file;
    }
    return undefined;
  }

  getChildFile(uid: string): CoffrePlainFile | undefined {
    let file = this.files.find(f => f.uid === uid);
    if (file instanceof CoffrePlainFile) {
      return file;
    }
    return undefined;
  }

  getFirstNonFullOrLastDirectory(): CoffreDirectory {
    let directory = this.mainDirectory;
    while (directory.nextDirectory) {
      if (directory.phyFiles.length < CoffreDirectory.maxFilePerDirectory) {
        break;
      }
      directory = directory.nextDirectory;
    }
    return directory;
  }
}
