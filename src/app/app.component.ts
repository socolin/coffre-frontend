import {Component, HostListener} from '@angular/core';
import {MsgAuth} from './app.model';
import {CoffreDirectory} from './files.model';
import {FilesService} from './files-service';
import {SafeControlService} from './safe-control.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public name: string = 'awerty';
  public passphrase: string = 'qweqwe';
  public action: string;
  public directoryData?: CoffreDirectory;

  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      this.safeControlService.close();
    }
  }

  constructor(public safeControlService: SafeControlService,
              public filesService: FilesService) {
  }

  connect(): void {
    this.safeControlService.connect().subscribe(() => {
      if (this.action === 'create') {
        this.safeControlService.createSafe(this.name, this.passphrase).subscribe((msg: MsgAuth) => {
          this.filesService.loadRootDirectory();
        });
        // this.name = '';
        // this.passphrase = '';
      } else if (this.action === 'auth') {
        this.safeControlService.authenticate(this.name, this.passphrase).subscribe((msg: MsgAuth) => {
          this.filesService.loadRootDirectory();
        });
        // this.name = '';
        // this.passphrase = '';
      }
    });
  }

  create(): void {
    this.action = 'create';
    this.connect();
  }

  authenticate(): void {
    this.action = 'auth';
    this.connect();
  }

  public onDrop(dropData: { files: FileList, directoryUid: string }) {
    for (let i = 0; i < dropData.files.length; i++) {
      let file = dropData.files[i];
      this.filesService.upload(dropData.directoryUid, file);
    }
  }
}
