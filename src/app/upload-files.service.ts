import {EventEmitter, Injectable} from '@angular/core';
import {CoffreDirectory, CoffreFile, CoffrePlainFile, FileState} from './files.model';
import {Msg, MsgAuth, MsgStartUpload, MsgUploadComplete} from './app.model';
import {UploadCoffreWebsocket} from './coffre-websocket';
import {SafeControlService} from './safe-control.service';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class UploadFilesService {
  static CHUNK_SIZE = 1024 * 256;
  static MAX_UPLOAD_FRAME = 5;
  private uploadWebsocket?: UploadCoffreWebsocket = undefined;
  private nextId = 1;
  private currentFile?: { id: number, file: File, directory: CoffreDirectory } = undefined;
  private pendingFiles: { id: number, file: File, directory: CoffreDirectory }[] = [];
  public fileStates: FileState[] = [];
  public currentFileState?: FileState = undefined;
  public uploadedFiles = 0;
  private sentUploadFrameCount = 0;
  private startDate: number;
  public speed: number;
  public onFileUploaded: EventEmitter<{ file: CoffreFile, directoryUid: string, askedDirectoryUid: string }> = new EventEmitter();
  private connected = false;

  constructor(private _safeControlService: SafeControlService) {
  }

  private connect(): Observable<Msg> {
    if (this.uploadWebsocket) {
      throw new Error('A connection is already established');
    }

    let onConnectSubject = new Subject<Msg>();

    this.uploadWebsocket = new UploadCoffreWebsocket(environment.wsUrl);

    this.uploadWebsocket.registerClose().subscribe((ev: CloseEvent) => {
      this.clear();
    });

    this._safeControlService.shareKey(2).subscribe((msg) => {
      if (!this.uploadWebsocket) {
        onConnectSubject.error(new Error('undefined upload websocket'));
        return;
      }

      this.uploadWebsocket.registerConnect().subscribe(
        (ev: Event) => {
          if (!this.uploadWebsocket) {
            onConnectSubject.error(new Error('undefined upload websocket'));
            return;
          }
          this.uploadWebsocket.sendPacket('auth-token', {token: msg.token}).subscribe((msgAuth: MsgAuth) => {
            this.connected = true;
            onConnectSubject.next(msgAuth);
            onConnectSubject.complete();
          }, err => {
            onConnectSubject.error(err);
          });
        },
        (err: Error) => {
          onConnectSubject.error(err);
        }
      );
      this.uploadWebsocket.connect();
    });

    return onConnectSubject;
  }

  public addFile(directory: CoffreDirectory, file: File) {
    this.pendingFiles.push({id: this.nextId, file: file, directory: directory});
    this.fileStates.push(new FileState(this.nextId, file.name, directory, file.size));
    this.nextId++;

    if (!this.currentFile) {
      this.startUpload();
    }
  }

  public cancelUpload(fileState: FileState) {
    fileState.canceled = true;
    fileState.state = 'CANCELED';
  }

  private onStartUpload(msg: MsgStartUpload) {
    if (!this.currentFileState) {
      return;
    }

    if (msg.success) {
      this.currentFileState.uid = msg.file_uid;
      this.currentFileState.state = 'UPLOADING';
      this.startDate = performance.now();
      this.continueUpload();
    } else {
      this.currentFileState.state = 'ERROR';
    }
  }

  private startUpload() {
    if (this.currentFile) {
      throw new Error('A file is already being uploaded');
    }

    if (!this.uploadWebsocket) {
      this.connect().subscribe((_: Msg) => {
        this.startUpload();
      });
      return;
    }

    if (!this.connected) {
      return;
    }

    do {
      if (this.pendingFiles.length === 0) {
        return;
      }

      this.currentFile = this.pendingFiles.shift();
      this.currentFileState = this.fileStates.find((f) => f.id === this.currentFile!.id);

      if (!this.currentFile || !this.currentFileState) {
        return;
      }
    } while (this.currentFileState.canceled);

    this.currentFileState.state = 'PREPARING';
    let notFullOrLast = this.currentFile.directory.getFirstNonFullOrLastDirectory();
    this.uploadWebsocket.sendStartUploadFile(this.currentFile.file.name, notFullOrLast.uid).subscribe((msg: MsgStartUpload) => {
      this.onStartUpload(msg);
    });
  }

  public clear() {
    this.uploadWebsocket = undefined;
    this.currentFile = undefined;
    this.pendingFiles = [];
    this.fileStates = [];
    this.currentFileState = undefined;
    this.sentUploadFrameCount = 0;
    this.speed = 0;
    this.connected = false;
  }

  private onUploadCancel(msg: Msg) {
    if (!this.currentFileState) {
      return;
    }

    if (msg.success) {
      this.currentFileState.canceled = true;
      this.currentFileState.state = 'CANCELED';
      this.currentFile = undefined;
      this.currentFileState = undefined;
      this.uploadedFiles++;
      this.speed = 0;
      this.startUpload();
    } else {
      this.currentFileState.state = 'ERROR';
    }
  }

  private onUpload(msg: Msg) {
    if (!this.currentFileState) {
      return;
    }
    if (!this.uploadWebsocket) {
      return;
    }

    if (msg.success) {
      this.sentUploadFrameCount--;
      this.currentFileState.transferredBytes += UploadFilesService.CHUNK_SIZE;
      if (this.currentFileState.transferredBytes >= this.currentFileState.sizeBytes) {
        this.currentFileState.transferredBytes = this.currentFileState.sizeBytes;
      }
      let now = performance.now();
      this.speed = this.currentFileState.transferredBytes * 1000 / (now - this.startDate);
      this.continueUpload();
    } else {
      this.currentFileState.state = 'ERROR';
    }
  }

  private finalizeUpload() {
    if (!this.currentFileState) {
      return;
    }
    if (!this.uploadWebsocket) {
      return;
    }

    if (this.sentUploadFrameCount > 0) {
      return;
    }

    this.currentFileState.state = 'FINISHING';

    this.uploadWebsocket.sendUploadComplete().subscribe((msg: Msg) => {
      this.onComplete(msg);
    });
  }

  private sendCancelUpload() {
    if (!this.currentFileState) {
      return;
    }
    if (!this.uploadWebsocket) {
      return;
    }

    if (this.sentUploadFrameCount > 0) {
      return;
    }

    this.uploadWebsocket.sendCancelUploadFile().subscribe((msg: Msg) => {
      this.onUploadCancel(msg);
    });
  }

  private onComplete(msg: Msg) {
    if (!(msg instanceof MsgUploadComplete)) {
      console.error('Invalid msg type');
      return;
    }

    if (!this.currentFileState) {
      return;
    }

    if (msg.success) {
      this.onFileUploaded.next({
        file: new CoffrePlainFile(
          this.currentFileState.uid,
          this.currentFileState.name,
          new Date().getTime() / 1000,
          this.currentFileState.sizeBytes,
          this.currentFileState.directory
        ),
        directoryUid: msg.directory_uid,
        askedDirectoryUid: this.currentFileState.directory.uid,
      });
      this.currentFileState.state = 'DONE';
      this.currentFile = undefined;
      this.currentFileState = undefined;
      this.uploadedFiles++;
      this.speed = 0;
      this.startUpload();
    } else {
      this.currentFileState.state = 'ERROR';
    }
  }

  private continueUpload() {
    if (this.sentUploadFrameCount >= UploadFilesService.MAX_UPLOAD_FRAME) {
      return;
    }

    if (!this.currentFile) {
      return;
    }

    if (!this.currentFileState) {
      return;
    }

    if (this.currentFileState.canceled) {
      this.sendCancelUpload();
      return;
    }

    if (this.currentFileState.sent >= this.currentFileState.sizeBytes) {
      this.currentFileState.sent = this.currentFileState.sizeBytes;
      this.finalizeUpload();
      return;
    }

    this.sentUploadFrameCount++;

    let blob = this.currentFile.file.slice(this.currentFileState.sent, this.currentFileState.sent + UploadFilesService.CHUNK_SIZE);

    this.currentFileState.sent += UploadFilesService.CHUNK_SIZE;
    if (this.currentFileState.sent >= this.currentFileState.sizeBytes) {
      this.currentFileState.sent = this.currentFileState.sizeBytes;
    }

    let reader = new FileReader();
    reader.onload = (ev: any) => {
      if (!this.uploadWebsocket) {
        return;
      }
      this.uploadWebsocket.sendUploadFile(Buffer.from(ev.target.result)).subscribe((msg: Msg) => {
        this.onUpload(msg);
      });
      this.continueUpload();
    };
    reader.readAsArrayBuffer(blob);
  }
}
